<?php

$error = false;

if(!empty($_POST['submitted'])) // check if button is clicked
{

    //get the values from the POST REQUEST
    $manufacturer = $_POST['Manufacturer'];$education_type = $_POST['selectedtyp'];
    $favorite_synthesis = $_POST['synthesis'];$name = trim($_POST['name']);   // trim the whitespaces
    $address = $_POST['address'];$agree = $_POST['agree'];$email = $_POST['email'];
    $phone = $_POST['phonenumber'];

    // set the error messages empty
    $name_error="";$select_type_error = "";$synthesis_error ="";$manufacturer_error = "";
    $terms_error = "";$address_error = "";$phone_error = "";$email_error = "";

    //if submitted, then validate
	if(empty($name))
	{
		$error=true;
        $name_error='Name is empty. Please enter your name.';
    }
    if(empty($address))
    {
        $error=true;
        $address_error='Address is empty. Please enter your name.';
    }
    if(empty($phone))
    {
        $error=true;
        $phone_error='Phone is empty. Please enter your name.';
    }
    if(empty($education_type))
	{
		$error=true;
        $select_type_error = "Please enter your type of education";
    }
	if(empty($favorite_synthesis))
	{
		$error=true;
        $synthesis_error ="Please enter your favorite synthesis";
	}
	if(empty($manufacturer) || count($manufacturer) < 2)
	{
		$error=true;
        $manufacturer_error = "Please select at least 2 items for Manufacturers";
    }
	if(empty($agree))
	{
		$error=true;
        $terms_error = "If you agree to the terms, please check the box below";
    }
    if(empty($email))
    {
        $email_error = "Please enter your email";
        $error=true;
    }else{
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //Unvalid email!
            $email_error = "Wrong email format";
            $error=true;
        }
    }

    if(false === $error)
	{
		//Validation Success!
		//Do form processing like email, database etc here
        mail("otti@petitcode.com", "Registrierung SAE", " NEUE REGISTRIERUNG " .$name , "From: Absender <register@sae.de>");
        header('Location: registered.html');
	}
}
?>
<!DOCTYPE html>
<html >
<head>
    <title>SAE EXAM Form</title>
    <link href="sae-exam-form.css" rel="stylesheet" type="text/css" />
</head>
<body >
<div id="wrap">
    <form method="post" action='sae-exam-form.php' id="saeform" >
        <div>
            <div class="cont_order">
                <fieldset>
                    <legend>Choose your education!</legend>
                    <span class='error'><?php echo $select_type_error; ?></span> <!-- here we are displaying the error -->
                    <div class='field_container'>
                        <label >Which education type?</label>
                        <label class='radiolabel'><input type="radio"  name="selectedtyp" value="RoundProducer" <?php echo ($education_type=='RoundProducer')? 'checked':''; ?> />Producer</label><br/>
                        <label class='radiolabel'><input type="radio"  name="selectedtyp" value="RoundInstrumentalist" <?php echo ($education_type=='RoundInstrumentalist')? 'checked':''; ?> />Instrumentalist</label><br/>
                        <label class='radiolabel'><input type="radio"  name="selectedtyp" value="RoundSoundDesigner" <?php echo ($education_type=='RoundSoundDesigner')? 'checked':''; ?> />SoundDesigner</label><br/>
                        <label class='radiolabel'><input type="radio"  name="selectedtyp" value="RoundMasterengineer" <?php echo ($education_type=='RoundMasterengineer')? 'checked':''; ?> />RoundMasterengineer</label><br/>
                    </div>

                    <div class='field_container'>
                        <label for="synthesis">Select favorite sound synthesis:</label >
                        <span class='error'><?php echo $synthesis_error; ?></span>
                        <select id="synthesis" name='synthesis' >
                            <option value="">Select synthesis</option>
                            <option <?php echo $favorite_synthesis=='Subtractive synthesis'?'selected':''; ?> >Subtractive synthesis</option> <!-- if it is the same result, we are writing selected as html -->
                            <option <?php echo $favorite_synthesis=='FM synthesis'?'selected':''; ?>>FM synthesis</option>
                            <option <?php echo $favorite_synthesis=='Granular synthesis'?'selected':''; ?>>Granular synthesis</option>
                            <option <?php echo $favorite_synthesis=='Wavetable synthesis'?'selected':''; ?>>Wavetable synthesis</option>
                            <option <?php echo $favorite_synthesis=='Physical Modeling'?'selected':''; ?>>Physical Modeling</option>
                        </select>
                    </div>
                    <div class='field_container'>
                        <label >Manufacturer (Choose at least 2!): </label>
                        <span class='error'><?php echo $manufacturer_error ?></span>
                        <label><input type="checkbox" value="EricaSynth" name='Manufacturer[]' <?php echo (in_array('EricaSynth',$manufacturer)) ?'checked':'' ?>  />EricaSynth</label>
                        <label><input type="checkbox" value="Moog" name='Manufacturer[]' <?php echo (in_array('Moog',$manufacturer)) ?'checked':'' ?>  />Moog</label>
                        <label><input type="checkbox" value="Dreadbox" name='Manufacturer[]' <?php echo (in_array('Dreadbox',$manufacturer)) ?'checked':'' ?>  />Dreadbox</label>
                        <label><input type="checkbox" value="Waldorf" name='Manufacturer[]' <?php echo (in_array('Waldorf',$manufacturer)) ?'checked':'' ?>  />Waldorf</label>
                        <label><input type="checkbox" value="DaveSmith" name='Manufacturer[]' <?php echo (in_array('DaveSmith',$manufacturer)) ?'checked':'' ?>  />DaveSmith</label>
                        <label><input type="checkbox" value="MFB" name='Manufacturer[]' <?php echo (in_array('MFB',$manufacturer)) ?'checked':'' ?>  />MFB</label>
                    </div>
                    <div class='field_container'>
                        <span class='error'><?php echo $terms_error ?></span> <!-- here we are displaying the error of the Terms Agreement -->
                        <label class="inlinelabel">
                            <input type="checkbox" id="agree" name='agree' <?php echo (empty($_POST['agree'])) ? '':'checked' ?> /> I agree to the <a href="javascript:void(0);">terms and conditions</a>
                        </label>
                    </div>
                </fieldset>
            </div>

            <div class="cont_details">
                <fieldset>
                    <legend>Contact Details</legend>
                    <label for='name'>Name</label>
                    <input type="text" id="name" name='name' value='<?php echo htmlentities($name) ?>' />
                    <span class='error'><?php echo $name_error ?></span>
                    <br/>
                    <label for='address'>Address</label>
                    <input type="text" id="address" name='address' />
                    <span class='error'><?php echo $address_error ?></span>
                    <br/>
                    <label for='phonenumber'>Phone Number</label>
                    <input type="text"  id="phonenumber" name='phonenumber'/>
                    <span class='error'><?php echo $phone_error ?></span>
                    <label for='email'>Email</label>
                    <input type="email"  id="email" name='email'/>
                    <span class='error'><?php echo $email_error ?></span>
                    <br/>
                </fieldset>
            </div>
            <input type='submit' id='submit' value='Submit' name='submitted' /> <!-- name is the attribute which will be sended -->
        </div>
    </form>
</div>
</body>
</html>
